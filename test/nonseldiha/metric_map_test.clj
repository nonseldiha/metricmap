(ns nonseldiha.metric-map-test
  (:require [nonseldiha.metric-map :as mm]
            [clojure.test :as t]
            [clojure.test.check :as tc]
            [clojure.test.check.generators :as tg]
            [clojure.test.check.properties :as tp]
            [clojure.test.check.clojure-test :as tt])
  (:import [nonseldiha.metric_map MetricMap]
           [clojure.lang ExceptionInfo]))

(set! *warn-on-reflection* true)

;; PURPOSE

;; test for metric-map's correctness via test generation
;; test the API
;; test if two metric-maps with different metadata and metrics are still `=`.



;; ANALYSIS

;; we need to ensure correctness of the 3 basic operations: get, assoc and dissoc
;; and of the public API: metric-map, inner-map, metrics.
;; and of equality semantics



;; DESIGN

;; properties of the map we will be testing:
;; - after an assoc we should have the value in the map and the corresponding :assoced   inc'd
;; - after a dissoc we should not have the value in map and the corresponding :dissoced  inc'd
;; - after a get that returns a value we should have the value in the map and :found     inc'd
;; - after a get that fails           we should have the value in the map and :not-found inc'd

;; for the API we will just do some simple checks


;; IMPLEMENTATION

(def non-nil              (tg/such-that some? tg/any))
(def any-map              (tg/map non-nil non-nil))
(def non-empty-map        (tg/such-that seq any-map))
(def any-metric-map       (tg/fmap mm/metric-map any-map))
(def non-empty-metric-map (tg/fmap mm/metric-map non-empty-map))

(def found-prop (tp/for-all [mm non-empty-metric-map]
                            (let [k             (-> mm keys rand-nth)
                                  k-metrics     (or (-> mm mm/metrics (get k)) mm/empty-metrics)
                                  v             (get mm k)
                                  bare-v        (-> mm mm/inner-map (get k))
                                  k-new-metrics (-> mm mm/metrics (get k))]
                              (and (= v bare-v)
                                   (= k-new-metrics (update k-metrics :found inc))))))

(def not-found-prop (tp/for-all [mm non-empty-metric-map]
                                (let [k             (loop [?k (rand) ks (-> mm mm/inner-map keys set)]
                                                      (if (ks ?k)
                                                        (recur (rand) ks)
                                                        ?k))
                                      k-metrics     mm/empty-metrics
                                      v             (get mm k ::nope)
                                      k-new-metrics (-> mm mm/metrics (get k))]
                                  (and (= ::nope v)
                                       (= k-new-metrics (update k-metrics :not-found inc))))))

(def assoc-prop (tp/for-all [mm any-metric-map
                             k  non-nil
                             v  non-nil]
                            (let [k-metrics     (or (-> mm mm/metrics (get k)) mm/empty-metrics)
                                  new-mm        (assoc mm k v)
                                  new-mm-k-v    (-> new-mm mm/inner-map (get k))
                                  k-new-metrics (-> new-mm mm/metrics (get k))]
                              (and (= v new-mm-k-v)
                                   (= k-new-metrics (update k-metrics :assoced inc))))))

(def dissoc-prop (tp/for-all [mm any-metric-map
                              k  non-nil]
                             (let [k-metrics     (or (-> mm mm/metrics (get k)) mm/empty-metrics)
                                   new-mm        (dissoc mm k)
                                   new-mm-k-v    (-> new-mm mm/inner-map (get k ::nope))
                                   k-new-metrics (-> new-mm mm/metrics (get k))]
                               (and (= ::nope new-mm-k-v)
                                    (= k-new-metrics (update k-metrics :dissoced inc))))))

(tt/defspec ?found     found-prop)
(tt/defspec ?not-found not-found-prop)
(tt/defspec ?assoc     assoc-prop)
(tt/defspec ?dissoc    dissoc-prop)


(t/deftest ?metric-map
  (t/is (instance? MetricMap (mm/metric-map)))
  (t/is (instance? MetricMap (mm/metric-map {:a 1})))
  (t/is (instance? MetricMap (mm/metric-map {} {:a {:found 0 :not-found 0 :assoced 0 :dissoced 0}})))
  (t/is (instance? MetricMap (mm/metric-map {:a 1} {:a {}})))
  (t/is (instance? MetricMap (mm/metric-map {:a 1} {} {})))
  (t/is (thrown? ExceptionInfo (mm/metric-map {:a 1} {:a "not-a-map"})))
  (t/is (thrown? Exception (assoc (mm/metric-map {} {:a "bad"}) :a 1)))
  )

(t/deftest ?inner-map
  (t/is (map? (-> {:a 1} mm/metric-map mm/inner-map)))
  (t/is (= {:a 1} (-> {:a 1} mm/metric-map mm/inner-map))))

(t/deftest ?metrics
  (t/is (= {:a {:found 1 :not-found 0 :assoced 1 :dissoced 0}}
           (mm/metrics (doto (assoc (mm/metric-map) :a 1)
                         (get :a))))))

(t/deftest ?=
  (t/is (= (mm/metric-map {:a 1})
           (doto (mm/metric-map {:a 1} {} {:so :meta})
             (get :a)))))
