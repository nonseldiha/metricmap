(ns nonseldiha.metric-map-bench
  (:require [nonseldiha.metric-map :as mm]
            [criterium.core :as bench]
            [clojure.test.check.generators :as tg])
  (:import [nonseldiha.metric_map MetricMap]
           [clojure.lang ExceptionInfo]))

(set! *warn-on-reflection* true)

;; (defn n-values [n]
;;   (tg/sample tg/any n))

;; (defn map-of-size-cca [n]
;;   (let [m (rand-int (* 2 n))]
;;     (zipmap (n-values m) (n-values m))))

(defn map-of-size [n]
  (let [^java.util.Map hm (java.util.HashMap. ^Long n)]
    (dotimes [i n]
      (.put hm (tg/generate tg/string-alphanumeric) (tg/generate tg/any)))
    (into {} hm)))

(map-of-size 10)

(defn bench-overhead-get [n]
  (let [m  (map-of-size n)
        mm (mm/metric-map m)
        stats #(bench/quick-benchmark (get % %2) {})
        mean #(-> % :mean first)
        random-key (-> m keys rand-nth)
        m-stats (stats m random-key)
        mm-stats (stats mm random-key)
        m-mean (mean m-stats)
        mm-mean (mean mm-stats)]
    ;; (clojure.pprint/pprint m-stats)
    ;; (clojure.pprint/pprint mm-stats)
    (double (/ mm-mean m-mean))))

(doseq [size [1 10 100 1000 10000]]
  (println size "\t->\t" (bench-overhead-get size)))

(comment
  overhead of get on a metric map
  left number is map size, right number is overhead, e.g. 5 means 5x slower
  1     ->  8.934579629749349
  10    ->  7.543773854314593
  100   ->  7.4299040402681715
  1000  ->  7.573343477159027
  10000 ->  7.563466848715163
  )

(defn bench-overhead-assoc [n]
  (let [m (map-of-size n)
        mm (mm/metric-map m)
        stats #(bench/quick-benchmark (assoc % %2 "bar") {})
        mean #(-> % :mean first)
        random-key (tg/generate tg/string-alphanumeric)
        m-stats (stats m random-key)
        mm-stats (stats mm random-key)]
    (double (/ (mean mm-stats) (mean m-stats)))))

(doseq [size [1 10 100 1000 10000]]
  (println size "\t->\t" (bench-overhead-assoc size)))

(comment
  overhead of assoc on a metric map
  left number is map size, right number is overhead, e.g. 5 means 5x slower
  1     ->  4.7683394335541935
  10    ->  3.9759056703161697
  100   ->  3.9172894405594954
  1000  ->  3.5750758783186893
  10000 ->  3.7103791007351123
  )

(defn bench-overhead-dissoc [n]
  (let [m (map-of-size n)
        mm (mm/metric-map m)
        stats #(bench/quick-benchmark (dissoc % %2) {})
        mean #(-> % :mean first)
        random-key (tg/generate tg/string-alphanumeric)
        m-stats (stats m random-key)
        mm-stats (stats mm random-key)]
    (double (/ (mean mm-stats) (mean m-stats)))))

(doseq [size [1 10 100 1000 10000]]
  (println size "\t->\t" (bench-overhead-dissoc size)))

(comment
  overhead of dissoc on a metric map
  left number is map size, right number is overhead, e.g. 5 means 5x slower
  1     ->	 13.72037866400974
  10    ->	 8.649048536261583
  100 	->	 6.465396101423569
  1000 	->	 6.970083336901085
  10000 ->	 6.929022641460519
  )
