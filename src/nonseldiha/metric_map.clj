(ns nonseldiha.metric-map
  (:require [potemkin]))

;; PURPOSE

;; A metric-map's purpose is to provide usage metrics about the map's keys
;; to allow drawing conclusions about the data usage.
;; An example use-case - you have a map and hand it off to someone else's code.
;; Once (s)he's done you can check if the values were used at all
;; or if they were used efficiently etc.

;; ANALYSIS

;; A metric-map is a map that holds usage metrics.
;; What it holds is the number of times a key:
;; - was found
;; - was not found (note this and previous combined is the number of gets)
;; - was assoced
;; - was dissoced

;; What it doesn't follow:
;; - with what values was a key found, i.e. "assoc 1, get, assoc 2, get" returned 1x 1 and 1x 2
;;   but we only track the found/not-found amount
;; - what values did a key have and how many times did it have that value

;; In other words it's just plain metrics,
;; not an event history you can source to get the map's different states
;; (which could be a nice, but different data structure)



;; DESIGN

;; The map is implemented via potemkin's def-map-type
;; which does the heavy lifting of fulfilling all the interfaces (there's a lot of them).
;; The metrics are held in a metrics field in an atom.
;; We need the atom since a get doesn't create a new map but affects the metrics,
;; so we effectively have to mutate.
;; You can create a fresh metric-map.
;; You can wrap an existing map to get metrics on its future usage.

;; Note that the underlying implementation (potemkin's def-map-type)
;; is a proxy to another map, which means we hold a real clojure map inside.
;; Clojure has multiple persistent map implementations,
;; e.g. a small map will be array backed and once it spills it turns into a hashy one.
;; By proxying the map we get all the benefits of these implementation design
;; without the need to code anything special.



;; IMPLEMENTATION

;; m   - a clojure map
;; mm  - a metric-map
;; mta - meta

(set! *warn-on-reflection* true)

(def ^:const empty-metrics {:found 0 :not-found 0 :assoced 0 :dissoced 0})

;; much faster than update-in
;; also when a key has no metrics we want to put all the 0s in
;; since we expect the user rather wants to get a number, even 0
;; than getting nil, therefore most of the calls would end up being
;; (-> mm metrics :a :not-found (or 0))
(defmacro incer [typ]
  (let [fresh-metrics (assoc empty-metrics typ 1)]
    `(fn ~(symbol (str "add-" (name typ))) [metrics# k#]
       (if-some [k%# (get metrics# k#)]
         (assoc metrics# k# (update k%# ~typ inc))
         (assoc metrics# k# ~fresh-metrics)))))

(def add-found     (incer :found))
(def add-not-found (incer :not-found))
(def add-assoced   (incer :assoced))
(def add-dissoced  (incer :dissoced))

(potemkin/def-map-type MetricMap [m metrics mta]
  (get [_ k default-value]
       (if-some [v (get m k)]
         (do (swap! metrics add-found k)
             v)
         (do (swap! metrics add-not-found k)
             default-value)))
  (assoc [_ k v]
         (MetricMap. (assoc m k v) (atom (add-assoced @metrics k)) mta))
  (dissoc [_ k]
          (MetricMap. (dissoc m k) (atom (add-dissoced @metrics k)) mta))
  (keys [_]
        (keys m))
  ;; potemkin defines this but from profiling it seemed to run in non-constant time
  (count [_]
         (count m))
  (meta [_]
        mta)
  (with-meta [_ mta]
    (MetricMap. m metrics mta)))

(defn- normalize-metrics [metrics m]
  (reduce (fn [mtrcs k]
            ;; if-some doesn't do a false check
            ;; so we save an if :)
            (if-some [k-metrics (get mtrcs k)]
              (if (map? k-metrics)
                (assoc mtrcs k (merge empty-metrics k-metrics))
                (throw (ex-info (str "supplied metrics for "
                                     (pr-str k)
                                     " must be a clojure map")
                                {:key k
                                 :value k-metrics
                                 :type (type k-metrics)})))
              (assoc mtrcs k empty-metrics)))
          metrics
          (keys m)))

(defn metric-map
  "Returns a MetricMap.

  With no arguments returns a fresh one.

  With one argument wraps an existing map.

  With two arguments wraps and adds starting metrics. The supplied metrics must be of form
  {:some-key {:found 1, :not-found 2, :assoced 1, :dissoced 0}, ...},
  i.e. all keys (:found, :not-found, :assoced, :dissoced) must be already present.
  Anything else is undefined behavior. The safest is to supply metrics from another `MetricMap`.

  With three arguments wraps, adds starting metrics and custom metadata.
  "
  ([]              (metric-map {}))
  ([m]             (metric-map m {}))
  ([m metrics]     (metric-map m metrics {}))
  ([m metrics mta] (MetricMap. m (atom (normalize-metrics metrics m)) mta)))

(defn metrics
  "Returns the metrics of `mm`."
  [^MetricMap mm]
  @(.metrics mm))

(defn inner-map
  "Returns the inner, bare, unmetriced map of `mm`."
  [^MetricMap mm]
  (.m mm))

(comment
  (metrics (doto (assoc (metric-map) :a 1) (get :a)))
  (require '[criterium.core :as bench]
           '[clj-java-decompiler.core :as dis]
           '[clj-async-profiler.core :as prof])
  (bench/quick-bench (reduce #(assoc % %2 %2) {} (range 1000)))
  (bench/quick-bench (reduce #(assoc % %2 %2) (metric-map) (range 1000)))
  (prof/profile {:transform (fn [s]
                              (clojure.string/replace s #"criterium\.[^;]+;" ""))}
                (bench/quick-bench (reduce #(assoc % %2 %2) (metric-map) (range 1000))))
  (prof/profile {:transform (fn [s]
                              (clojure.string/replace s #"criterium\.[^;]+;" ""))}
                (bench/quick-bench (reduce (fn [mm v]
                                             ;; (when (-> mm count (mod 100) zero?)
                                             ;;   (metrics mm))
                                             (assoc mm v v))
                                           (metric-map)
                                           (range 1000))))
  (prof/serve-files 8080)
  (= (metric-map) (doto (metric-map) (get :a)))
  (metrics (doto (metric-map {:a 1 :b 2}) (get :a)))
  )

;; TODO benchmarks for get/assoc/dissoc on small, medium and large maps
;; pure clojure map       - 280us
;; atom with clojure map  - 830us
;;
;; so it takes ~3x as much time. This is close to expectations
;; since we are bookkeeping a second map in an atom
;;
;; using mutable java types to do the bookkeeping reduces it to 1.5x
;; but the resulting object behaves as a mutable one wrt metrics
;;
;; to get immutable semantics one would have to deep-copy things on each assoc/dissoc
;; which would be even slower



;; LTS

;; see tests/nonseldiha/metric_map.clj
